package com.example.task.Utils;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {


    private static RestInterface restApiInterface;
    private static OkHttpClient client = null;


    public static void setClient() {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.MINUTES)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .writeTimeout(10, TimeUnit.MINUTES)
                    .build();
        }
    }

    public static RestInterface getRestApiInterface() {
        if (restApiInterface == null) {
            /*initialize okHttp client*/
            setClient();

            Retrofit rest = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(restApiInterface.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            restApiInterface = rest.create(RestInterface.class); /*retrofit creates implementations for the interface*/

        }
        return restApiInterface;
    }


}