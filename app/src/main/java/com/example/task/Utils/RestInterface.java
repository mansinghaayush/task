package com.example.task.Utils;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RestInterface {

    String BASE_URL = "https://jsonplaceholder.typicode.com/";
    @GET("photos")
    Call<ArrayList<Photo>> getPhotos();

}
