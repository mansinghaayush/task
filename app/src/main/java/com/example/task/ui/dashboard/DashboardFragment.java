package com.example.task.ui.dashboard;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.BottomNavActivity;
import com.example.task.Login.LoginActivity;
import com.example.task.R;
import com.example.task.Utils.Photo;
import com.example.task.Utils.RestClient;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    androidx.appcompat.widget.Toolbar toolbar;
    private ProgressDialog mProgress;
    private RecyclerView recyclerView;
    private ArrayList<Photo> photoList;
    View root;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_dashboard, container, false);


        toolbar = root.findViewById(R.id.toolbar_main);
        drawerLayout = root.findViewById (R.id.drawer);

        /*initialize side navigation bar*/
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,toolbar,R.string.open_drawer,R.string.close_drawer);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true); //enable hamburger
        drawerToggle.syncState();

        /*navigation item selected listener for log out*/
        NavigationView navigationView = root.findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(item -> {
            if(item.getItemId() == R.id.nav_sign_out){
                Toast.makeText(getContext(), "Logging Out", Toast.LENGTH_LONG).show();
                SharedPreferences sp = this.getActivity().getSharedPreferences("UserPrefs", 0);
                sp.edit().remove("email").apply();
                sp.edit().remove("password").apply();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                new BottomNavActivity().finish();
                return true;
            }
            return true;
        });

        /*display email address and profile picture on navigation bar*/
        TextView displayEmailTxt = root.findViewById(R.id.displayEmailTxt);
        ImageView sideNavImg;
        SharedPreferences sp = getActivity().getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
        String image = sp.getString("imageUri", "");
        Uri imageUri = Uri.parse(image);
        String email = sp.getString("email", "");
        View headerView = navigationView.getHeaderView(0);
        sideNavImg = (ImageView) headerView.findViewById(R.id.sideNavImg);
        displayEmailTxt = (TextView) headerView.findViewById(R.id.displayEmailTxt);
        displayEmailTxt.setText(email);

        if(imageUri.equals(null)) {
            sideNavImg.setImageURI(imageUri);
        }
        /* show progress bar*/
        initProgressDialog();
        /*fetch data from API and show it in recycler view*/
        fetchData();
        return root;



    }

    private void initProgressDialog(){

        mProgress = new ProgressDialog(getActivity());
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setMessage("Loading...");

    }


    private void fetchData() {
        mProgress.show();

        Call<ArrayList<Photo>> call = RestClient.getRestApiInterface().getPhotos();
        call.enqueue(new Callback<ArrayList<Photo>>() {
            @Override
            public void onResponse(Call<ArrayList<Photo>> call, Response<ArrayList<Photo>> response) {
                mProgress.dismiss(); /*dismisses the loading progress bar*/
                photoList = response.body();
                recyclerView = root.findViewById(R.id.recycler_view);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(new DashboardAdapter(getActivity(), photoList));

            }

            @Override
            public void onFailure(Call<ArrayList<Photo>> call, Throwable t) {
                Toast.makeText(getContext(), "Unsuccessful Response from API", Toast.LENGTH_LONG).show();
            }
        });
    }


}