package com.example.task.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.task.R;
import com.squareup.picasso.Picasso;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {
    TextView profileEmailTxt;
    ImageView profileImg;
    Button changePictureBtn;
    private static int RESULT_LOAD_IMAGE = 1;
    SharedPreferences sp;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        profileEmailTxt = root.findViewById(R.id.profileEmailTxt);
        SharedPreferences sp = getActivity().getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        profileEmailTxt.setText(email);

        profileEmailTxt = root.findViewById(R.id.profileEmailTxt);
        profileImg = root.findViewById(R.id.profileImg);
        changePictureBtn = root.findViewById(R.id.changePictureBtn);

        changePictureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                            }
        });

        sp = getActivity().getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
        String image = sp.getString("imageUri", "");
        Uri imageUri = Uri.parse(image);
        if(imageUri.equals(Uri.parse(""))) {
            profileImg.setImageURI(imageUri);
        }
                return root;


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            sp = this.getActivity().getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("imageUri", selectedImage.toString());
            editor.apply();
            profileImg.setImageURI(selectedImage);

        }
    }
}