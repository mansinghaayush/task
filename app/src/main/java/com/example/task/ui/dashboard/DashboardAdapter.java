package com.example.task.ui.dashboard;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.Utils.Photo;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private ArrayList<Photo> photoList;
    private Activity mActivity;

    public DashboardAdapter(Activity mActivity, ArrayList<Photo> photoList) {
        this.photoList = photoList;
        this.mActivity = mActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.content_holder_layout,parent,false);
        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Photo thisPhoto = photoList.get(position);

        holder.text1.setText("Album ID: "+ thisPhoto.getAlbumId());
        holder.text2.setText("ID: " + thisPhoto.getId());
        holder.text3.setText(thisPhoto.getTitle());
        holder.text4.setText(thisPhoto.getUrl());
        Picasso.get().load(thisPhoto.getThumbnailUrl()).into(holder.thumbnailImg);

    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView text1, text2, text3, text4;
        ImageView thumbnailImg;

        public ViewHolder(View itemView){
            super(itemView);

            text1 = itemView.findViewById(R.id.albumTxt);
            text2 = itemView.findViewById(R.id.idTxt);
            text3 = itemView.findViewById(R.id.titleTxt);
            text4 = itemView.findViewById(R.id.urlTxt);
            thumbnailImg = itemView.findViewById(R.id.thumbnailImg);
        }

    }

}

