package com.example.task;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.Login.LoginActivity;
import com.example.task.Utils.Photo;
import com.example.task.Utils.RestClient;
import com.example.task.ui.dashboard.DashboardAdapter;
import com.google.android.material.navigation.NavigationView;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    androidx.appcompat.widget.Toolbar toolbar;
    private ProgressDialog mProgress;
    private RecyclerView recyclerView;
    private ArrayList<Photo> photoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_dashboard);

        toolbar = findViewById(R.id.toolbar_main);
        drawerLayout = findViewById (R.id.drawer);

        /*initialize side navigation bar*/
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar,R.string.open_drawer,R.string.close_drawer);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true); //enable hamburger
        drawerToggle.syncState();

        /*navigation item selected listener for log out*/
        NavigationView navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(item -> {
            if(item.getItemId() == R.id.nav_sign_out){
                Toast.makeText(getApplicationContext(), "Logging Out", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            }
            return true;
        });

        /*display email address on navigation bar*/
        TextView displayEmailTxt = findViewById(R.id.displayEmailTxt);
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
        String email = sp.getString("email", "");
        View headerView = navigationView.getHeaderView(0);
        displayEmailTxt = (TextView) headerView.findViewById(R.id.displayEmailTxt);
        displayEmailTxt.setText(email);

       /* show progress bar*/
        initProgressDialog();
        /*fetch data from API and show it in recycler view*/
        fetchData();
    }



    private void initProgressDialog(){

        mProgress = new ProgressDialog(this);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setMessage("Loading...");

    }


    private void fetchData() {
        mProgress.show();

        Call<ArrayList<Photo>> call = RestClient.getRestApiInterface().getPhotos();
        call.enqueue(new Callback<ArrayList<Photo>>() {
            @Override
            public void onResponse(Call<ArrayList<Photo>> call, Response<ArrayList<Photo>> response) {
                    mProgress.dismiss(); /*dismisses the loading progress bar*/
                    photoList = response.body();
                    recyclerView = findViewById(R.id.recycler_view);
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashboardActivity.this));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(new DashboardAdapter(DashboardActivity.this, photoList));

            }

            @Override
            public void onFailure(Call<ArrayList<Photo>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Unsuccessful Response from API", Toast.LENGTH_LONG).show();
            }
        });
    }



}
