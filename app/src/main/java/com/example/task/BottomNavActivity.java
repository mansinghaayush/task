package com.example.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.task.Login.LoginActivity;
import com.example.task.Utils.Photo;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BottomNavActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    androidx.appcompat.widget.Toolbar toolbar;
    private ProgressDialog mProgress;
    private RecyclerView recyclerView;
    private ArrayList<Photo> photoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

//        toolbar = findViewById(R.id.toolbar_main);
//        drawerLayout = findViewById (R.id.drawer);
//
//        /*initialize side navigation bar*/
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar,R.string.open_drawer,R.string.close_drawer);
//        drawerLayout.addDrawerListener(drawerToggle);
//        drawerToggle.setDrawerIndicatorEnabled(true); //enable hamburger
//        drawerToggle.syncState();
//
//        /*navigation item selected listener for log out*/
//        NavigationView navigationView = findViewById(R.id.navigationView);
//        navigationView.setNavigationItemSelectedListener(item -> {
//            if(item.getItemId() == R.id.nav_sign_out){
//                Toast.makeText(getApplicationContext(), "Logging Out", Toast.LENGTH_LONG).show();
//
//                Intent intent = new Intent(BottomNavActivity.this, LoginActivity.class);
//                startActivity(intent);
//                finish();
//                return true;
//            }
//            return true;
//        });
//
//        /*display email address on navigation bar*/
//        TextView displayEmailTxt = findViewById(R.id.displayEmailTxt);
//        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
//        String email = sp.getString("email", "");
//        View headerView = navigationView.getHeaderView(0);
//        displayEmailTxt = (TextView) headerView.findViewById(R.id.displayEmailTxt);
//        displayEmailTxt.setText(email);

    }

}