package com.example.task.Login;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task.BottomNavActivity;
import com.example.task.DashboardActivity;
import com.example.task.R;

public class LoginActivity extends AppCompatActivity {

    EditText emailAddress, password;
    String emailStr, passwordStr;
    Button logInBtn;
    SharedPreferences sp;
    public final static String TEST_EMAIL = "test";
    public final static String TEST_PASSWORD = "test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        emailAddress = findViewById(R.id.emailTxt);
        password = findViewById(R.id.passwordTxt);

        logInBtn = findViewById(R.id.logInBtn);
        sp = getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);

        /*on click listener for log in button*/
        logInBtn.setOnClickListener(view -> {
            emailStr = emailAddress.getText().toString();
            passwordStr = password.getText().toString();
            if (emailStr.equals(TEST_EMAIL) && passwordStr.equals(TEST_PASSWORD)) {
                saveCredentials();
                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Incorrect Username or Password", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void saveCredentials() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("email", emailStr);
        editor.putString("password", passwordStr);
        editor.apply();
    }


}

